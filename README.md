# Centro de Excelencia e Innovación Tecnológica de Bioimagen de la Conselleria de Sanitat #

[http://ceib.san.gva.es/](http://ceib.san.gva.es/)

## Banco digital de Imagen Médica de la Comunidad Valenciana | BIMCV ##

[http://ceib.san.gva.es/bimcv](http://ceib.san.gva.es/bimcv)

[https://ceib.bioinfo.cipf.es/xnat/](https://ceib.bioinfo.cipf.es/xnat/)

### Subdirección General de Sistemas de Información para la Salud ###

#### Conselleria de Sanitat Universal i Salut Pública ####

[http://www.san.gva.es/](http://www.san.gva.es/)

**Team leader**: María de la Iglesia Vayá | maigva AT gmail DOT com

Ángel Fernández-Cañada Vilata | anferca83v2 AT gmail DOT com

Jorge Isnardo Altamirano | jisnardo88 AT gmail DOT com